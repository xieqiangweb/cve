 **Rockoa v2.3.2 has a file upload vulnerability** 

Vulnerability file：qcloudCosAction.php

1. Log in to the background to find any file upload point
![输入图片说明](WPS%E5%9B%BE%E7%89%87(6).png)
In fact, the upload point is the same, the call is the same interface.
![输入图片说明](WPS%E5%9B%BE%E7%89%87(2).png)

2.The uploaded file suffix of.php is replaced with the suffix of.uptemp and stored in the database.
![输入图片说明](WPS%E5%9B%BE%E7%89%87(3).png)
![输入图片说明](WPS%E5%9B%BE%E7%89%87(7).png)

3.While searching for the function point, I found a way to decrypt the base64 file, and fetched the corresponding data from the database according to the id. Replace the suffix of the original file with the suffix of the uploaded file, that is, the uploaded.php.
![输入图片说明](WPS%E5%9B%BE%E7%89%87(4).png)

4. Access the function point through the route

![输入图片说明](WPS%E5%9B%BE%E7%89%87(5).png)

5.Replace the previous upload id value
![输入图片说明](WPS%E5%9B%BE%E7%89%87(8).png)
Found that the file has been replaced with a suffix
![输入图片说明](WPS%E5%9B%BE%E7%89%87(9).png)
![输入图片说明](WPS%E5%9B%BE%E7%89%87(1).png)