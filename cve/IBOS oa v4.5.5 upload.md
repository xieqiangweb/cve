 **IBOS v4.5.5 File upload bypass** 

download link:https://gitee.com/ibos/IBOS

You can't set the.php suffix here, but you can bypass it by setting the.htaccess suffix.

![输入图片说明](image-20221013172311756(1).png)

The.htaccess content is as follows

![输入图片说明](image-20221013172422761(2).png)

Then we will upload a 1.gif file containing one sentence

![输入图片说明](image-20221013172559260(3).png)

Finally, visit 1.gif to get getshell

![输入图片说明](image-20221013174638894(4).png)
