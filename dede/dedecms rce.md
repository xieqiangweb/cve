 **DEDECMS v5.7.106 has a command execution vulnerability** 

In the uploads/dede/config.php file, we find that the file written in the UpDateMemberModCache() function is.inc, but the content written here is obtained from the database, and the database content is under our control.

The failure to include the global filter function config.php is the key vulnerability

![输入图片说明](%E5%9B%BE%E7%89%87.png)

The file is written to config_passport.php. We can add the content we want through the membership model management function point. After adding the content in the database is as follows

![输入图片说明](WPS%E5%9B%BE%E7%89%87(4).png)

![输入图片说明](WPS%E5%9B%BE%E7%89%87(5).png)

Let's take a look at the member model.inc file

![输入图片说明](WPS%E5%9B%BE%E7%89%87(6).png)

We successfully escape our content by escaping the single quotation mark with the \ backslash, closing it with the following single quotation mark, and then closing all the preceding content with parentheses.

![输入图片说明](WPS%E5%9B%BE%E7%89%87(7).png)

View the call to UpDateMemberModCache

This function is used in dede/member_model_add.php. After testing, it is found that if a new member template is added through member model management, the content will be written to the member_model.inc file

![输入图片说明](WPS%E5%9B%BE%E7%89%87(8).png)


inc file cannot be used, we go to the system function point to find a place where we can edit the file, and then include member_model.inc to successfully getshell.



Create a new file through the file manager, and then include the.inc file we just did, so that we can successfully bypass the malicious function detection of dede, and finally getshell.

![输入图片说明](WPS%E5%9B%BE%E7%89%87(9).png)

Access the 1.php file

![输入图片说明](WPS%E5%9B%BE%E7%89%87(10).png)